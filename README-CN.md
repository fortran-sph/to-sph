# SPH 前后处理 / to-sph

SPH / Ti-SPH 下属程序，前后处理，构建初始化粒子文件 PIF.h5part。

使用本程序，可以实现从 Lua 脚本中获取初始化粒子方法，未来将支持部分 CAE 软件（如 MSC.Patran ）。

[![to-sph](https://img.shields.io/badge/To--SPH-v0.4.1-blueviolet)][1]
[![ford](https://img.shields.io/badge/Docs-FORD-ff69b4)][5]
[![license](https://img.shields.io/badge/License-MIT-important)](LICENSE)
[![wakatime](https://wakatime.com/badge/user/ca8e3153-da86-47e8-ba89-1fac0c842c19/project/e1c952ae-644e-451b-bf8f-dc9d7342fee2.svg)][4]
[![fpm](https://img.shields.io/badge/Fortran--lang/fpm-v0.7.0-blue)][2]
[![Compiler](https://img.shields.io/badge/Compiler-GFortran^10.3.0-brightgreen)][3]

[1]: https://gitee.com/fortran-sph/to-sph
[3]: https://fortran-lang.org/compilers/
[4]: https://wakatime.com/@zoziha/projects/knetpjepgp

```sh
export FPM_FFLAGS="-Ipath/to/hdf5&lua/include"
fpm install --profile release
fpm run -- --help
```

## 依赖

- Lua >= 5.3
- HDF5 > 1.10.6
- [GNU/GCC Fortran][1] >= 10.3.0：用于编译 Fortran 代码；
- [Fortran-lang/fpm][2] >= 0.7.0：用于构建编译过程。

备注：作者使用 Windows-MSYS2 环境开发，其他环境需要自行验证。

[1]: https://gcc.gnu.org/
[2]: https://github.com/fortran-lang/fpm

## 使用 fpm 构建

```powershell
$env:FPM_FFLAGS="-Ipath/to/include -Lpath/to/lib"
fpm install --profile release
fpm run -- --help
```

单元测试：

```powershell
fpm test
```

## API 文档

```sh
ford FORD-project-file.md
start ./API-html/index.html     # Windows 打开网页
```

最新的 API 文档，详见 [presph-api][5] 。

[5]: https://pre-process-api.netlify.app/