title: h5part
---

## h5part

H5part 是一种具体结构的 HDF5 文件，可用于数据传输、存储和可视化，`*.h5part` 可被 ParaView 以 H5part 文件解析，
适用于粒子模拟场景。
