title: HDF5 和 Lua 依赖设置
---

`To-SPH` 采用外部依赖 Lua 和 HDF5 用于粒子初始化与存储。这在 Windows-MSYS2 和 Arch Linux 环境下很容易满足，但是在一些其他 Linux 发行版下，需要额外的设置，主要是利用软连接实现依赖链接库的满足。

## 安装 HDF5

在 Windows-MSYS2/Arch Linux 中，可以简便地安装最新版本的 HDF5。
而在 Ubuntu Linux 下，仓库目前的 HDF5 版本还保留在 1.0.4，与 `Ti-SPH` 用到的 `h5part` 库存在兼容性编译问题，
需要另外从 [HDF5 仓库][1]中下载安装，下载解压后，运行如下命令：

```sh
cmake -B build -DCMAKE_INSTALL_PREFIX=/usr/local/ && cd build
sudo make install -j2   # 根据自己的需求设定并行编译线程
```

在 Linux 下安装的 HDF5 需要额外添加链接库路经，传递给 fpm 构建工具和 LD_LIBRARY_PATH 环境变量，以下提供两种方式：

1. 设置 `FPM_CFLAGS` 和 `FPM_FFLAGS` 环境变量；
2. 使用命令行选项：
```sh
fpm build --c-flag "-I? -L?" --flag "-I? -L?"    # ? 处填写头文件与链接库路径
```

可行的方法是创建软连接：

```sh
sudo ln -s /usr/local/lib/libhdf5.so.1000 /usr/lib/x86_64-linux-gnu/libhdf5.so.1000
sudo ln -s /usr/local/lib/libhdf5_hl.so.1000 /usr/lib/x86_64-linux-gnu/libhdf5_hl.so.1000
sudo ln -s /usr/lib/x86_64-linux-gnu/liblua5.3.so.0 /usr/lib/x86_64-linux-gnu/liblua.so
```

[1]: https://github.com/HDFGroup/hdf5
    