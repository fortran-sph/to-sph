title: hdf5 简单教程
---

## h5dump

```pwsh
>> h5dump -d /n ./example/pif.h5
HDF5 "./example/pif.h5" {
DATASET "/n" {
   DATATYPE  H5T_STD_I32LE
   DATASPACE  SCALAR
   DATA {
   (0): 1600
   }
}
}
>> h5dump -a /Kind .\example\tmpin\PIF.h5part
```
