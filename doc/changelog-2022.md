title: 日志 2022
---

## 日志

- [x] 适应新版 M_CLI2；
- [x] 使用 hdf5+h5part 作为数据存储后端，取代 h5fortran，可用于 paraview 可视化；
- [x] 在 h5part 和 sph.toml 中控制精度！
- [x] 更新依赖；
- [x] 重构 CLI;
- [x] 使用 namelist 存储属性；
- [x] 程序日志输出在 `~/.cache/` 下；
- [x] 提供 powershell 脚本以实现 ci；

## 待办

- [ ] 支持更多的数据结构;
- [ ] 支持读取 TOML;
- [ ] 支持 `--skip` 参数，可以跳过某些步骤；
