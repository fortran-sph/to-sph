---
project: to-sph
summary: sph / ti-sph 前后处理程序
project_website: https://gitee.com/fortran-sph/to-sph
project_download: https://gitee.com/fortran-sph/to-sph/releases
output_dir: API-html
page_dir: doc
src_dir: src
         app
media_dir: doc/media
md_extensions: markdown.extensions.toc
author: 左志华
author_description: 哈尔滨工程大学-船舶与海洋结构物设计制造，在读学生
email: zuo.zhihua@qq.com
author_pic: ./media/zoziha.png
website: https://gitee.com/zoziha
preprocess: false
display: public
source: true
parallel: 4
print_creation_date: true
creation_date: %Y-%m-%d %H:%M %z
---

{!README-CN.md!}
