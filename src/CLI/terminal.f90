!! 利用 ESC 码和环境变量、ISATTY 设置终端颜色

!> author: 左志华
!> date: 2022-07-14
!> version: beta
!>
!> Terminal <br>
!> 终端文本，给出带样式的终端文字，更友好的用户 CLI 界面
!> @note 手动设置环境变量 NO_COLOR=1 可以关闭终端文本样式
module sph_terminal

    use tomlf_terminal, only: toml_terminal, operator(//)
    use seakeeping_utils, only: isatty, env_color
    implicit none

    public :: to_sph_terminal, set_terminal, warn, info, error

    type(toml_terminal) :: to_sph_terminal  !! to-sph Terminal <br>
                                            !! to-sph 终端

contains

    !> By TTY terminal and NO_COLOR environment variable, enable console color <br>
    !> 根据 TTY 终端和 NO_COLOR 环境变量，启用控制台颜色
    subroutine set_terminal()
        to_sph_terminal = toml_terminal(merge(.true., .false., isatty() == 1) &
                                        .and. env_color())
    end subroutine set_terminal

    !> Warn <br>
    !> 警告
    pure function warn(msg) result(ans)
        character(*), intent(in) :: msg     !! Input message <br>
                                            !! 输入信息
        character(:), allocatable :: ans

        ans = to_sph_terminal%yellow//msg//to_sph_terminal%reset

    end function warn

    !> Info <br>
    !> 信息
    pure function info(msg) result(ans)
        character(*), intent(in) :: msg     !! Input message <br>
                                            !! 输入信息
        character(:), allocatable :: ans

        ans = to_sph_terminal%blue//msg//to_sph_terminal%reset

    end function info

    !> Error <br>
    !> 错误，红色
    pure function error(msg) result(ans)
        character(*), intent(in) :: msg     !! Input message <br>
                                            !! 输入信息
        character(:), allocatable :: ans

        ans = to_sph_terminal%red//msg//to_sph_terminal%reset

    end function error

    !> 输入
    pure function input(msg) result(ans)
        character(*), intent(in) :: msg     !! Input message <br>
                                            !! 输入信息
        character(:), allocatable :: ans

        ans = to_sph_terminal%green//msg//to_sph_terminal%reset

    end function input

end module sph_terminal
