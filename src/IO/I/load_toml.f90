!> author: 左志华
!> date: 2022-07-19
!>
!> Load toml file <br>
!> 读取 toml：<br>
!> 输入：1-pif.lua->h5part（默认）；<br>
!> 核心：pif.h5part（默认）；<br>
!> 输出：1-pif.h5part->二进制 VTU。
!> @todo
module sph_toml_input_m

    use tomlf, only: toml_table, get_value, toml_parse
    use seakeeping_logger, only: stdlog => global_logger
    use seakeeping_error_handling, only: file_not_found_error
    use seakeeping_string, only: to_string
    use seakeeping_filesystem, only: is_exist
    implicit none

    public :: get_project_configuration

contains

    !> Load toml file <br>
    !> 读取前处理配置
    !> @todo 完善配置文件的读取
    subroutine get_project_configuration(toml_file)
        character(:), intent(out), allocatable :: toml_file     !! File name <br>
                                                                !! 文件名
        type(toml_table), allocatable :: ini_table
        type(toml_table), pointer :: subtable

    
        if (.not. is_exist(toml_file)) call file_not_found_error(toml_file)

        open (1, file=toml_file, status='old')
        call toml_parse(ini_table, 1)
        close (1)

        call get_value(ini_table, 'To-SPH', subtable)

        nullify (subtable)
    end subroutine get_project_configuration

end module sph_toml_input_m
