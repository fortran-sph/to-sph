!> author: 左志华
!> date: 2022-07-20
!>
!> Pre/Post-process the data <br>
!> 前后处理
module sph

    use seakeeping_filesystem, only: operator(.join.)
    use seakeeping_error_handling, only: fatal_error
    use seakeeping_filesystem, only: is_exist
    use sph_command_line, only: pre_settings_t, post_settings_t
    use sph_region_type, only: region_t
    use sph_save_h5part, only: save_h5part
    use sph_load_h5part, only: load_h5part
    use sph_load_lua, only: load_lua_script
    use sph_terminal, only: info, error
    use sph_save_vtk, only: save_vtk
    use sph_terminal, only: input

contains

    !> Pre process the data <br>
    !> 前处理数据
    subroutine cmd_pre(cmd_settings, region)
        type(pre_settings_t), intent(in) :: cmd_settings    !! Pre process settings <br>
                                                            !! 前处理设置
        type(region_t), intent(out) :: region               !! Region <br>
                                                            !! 粒子域
        select case (cmd_settings%type)
        case ("lua", "LUA")
            write (*, '(a)') info('Pre-processing: Load lua script ...')
            call load_lua_script(cmd_settings%working_dir.join.'pif.lua', region)

        case default
            call fatal_error("Unsupported pre-processing type: "//cmd_settings%type)

        end select

        write (*, '(a)') info('Pre-processing: Save h5part file ...')
        call save_h5part(file=cmd_settings%working_dir.join.'pif.h5part', &
                         nml=cmd_settings%working_dir.join.'pif.nml', &
                         region=region, &
                         skip=cmd_settings%skip)

    end subroutine cmd_pre

    !> Post process for the results of the simulation <br>
    !> 对结果的后期处理
    subroutine cmd_post(cmd_settings, region)
        type(post_settings_t), intent(in) :: cmd_settings   !! Post process settings <br>
                                                            !! 后期处理设置
        type(region_t), intent(out) :: region               !! Region <br>
                                                            !! 计算域
        logical :: cover
        integer :: stat

        select case (cmd_settings%type)
        case ("vtk", "VTK")
            if (is_exist(cmd_settings%working_dir.join.'pif1.vtu') .and. .not. cmd_settings%skip) then
                write (*, '(a)', advance='no') input('The file already exists, will the original file be overwritten? (t/f): ')
                read (*, *, iostat=stat) cover
                if (.not. cover .or. stat /= 0) return
            end if

            write (*, '(a)') info('Post-processing: Load h5part file ...')
            write (*, '(a)') info('Post-processing: save vtk file ...')
            call load_h5part(file=cmd_settings%working_dir.join.'pif-out.h5part', &
                             nml=cmd_settings%working_dir.join.'pif.nml', &
                             vtk=cmd_settings%working_dir, &
                             region=region, &
                             func=save_vtk)

        case default
            call fatal_error('Unsupported post-processing type: '//cmd_settings%type)

        end select

    end subroutine cmd_post

end module sph
